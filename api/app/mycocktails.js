const express = require('express');
const auth = require('../middleware/auth');
const mongoose = require('mongoose');
const permit = require("../middleware/permit");
const MyCocktail = require("../models/Mycocktail");
const router = express.Router();

router.post('/', auth, async (req, res , next) => {
  try{
    const MyCocktailData = {
      user: req.user._id,
      cocktail: req.body.cocktail,
      isPublished: false
    }

    const cocktail = new MyCocktail(MyCocktailData);
    await cocktail.save();
    return res.send(cocktail);
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError){
      return res.status(400).send(e);
    }
    return next(e);
  }

});

router.get('/', auth,  async (req, res , next) => {
  try{
    const myCocktail = await MyCocktail.find({user: req.user._id}).populate('cocktail', 'title image');

    return res.send(myCocktail);
  }catch (e) {
    next(e);
  }
});


module.exports = router;