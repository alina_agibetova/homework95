const express = require('express');
const Cocktail = require('../models/Cocktail');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const {cocktail, uploads} = require("../multer");

const router = express.Router();

router.get('/', auth, async (req, res, next) => {
  try{
    const cocktails = await Cocktail.find().populate('ingredients', 'name amount');
    return res.send(cocktails);
  }catch (e) {
    next(e);
  }
});

router.get('/:id', auth,  async (req, res, next) => {
  try{
    const cocktail = await Cocktail.findById( req.params.id);

    if (!cocktail){
      return res.status(404).send({message: 'Not found'});
    }

    if (req.user.role === 'admin'){
      req.body.isPublished = true;
    }

    return res.send(cocktail);
  } catch (e){
    next(e);
  }
});

router.post('/', auth, permit('admin', 'user'), uploads.single('image'), async (req, res, next) => {
  try{
    const cocktailData = {
      user: req.body.user,
      title: req.body.title,
      image: null,
      recipe: req.body.recipe,
      ingredients: JSON.parse(req.body.ingredients),
      isPublished: false
    };

    if (req.file){
      cocktailData.image = req.file.filename;
    }

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();
    return res.send({message: 'Created new cocktail', id: cocktail._id})
  }catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try{
    const publish = await Cocktail.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published', publish});
  } catch (e) {
    next(e)
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
  try{
    if (req.params.id){
      const cocktail = await Cocktail.deleteOne({_id: req.params.id});

      return res.send(cocktail);
    }
  } catch (e){
    next(e);
  }
});

module.exports = router;