const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public'),
  mongo: {
    db: 'mongodb://localhost/homework95',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '731477771540843',
    appSecret: 'a92f61a1e9f6643de3ee6f7e093d9824'
  }
}

