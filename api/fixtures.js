const mongoose = require('mongoose');
const config = require('./config');
const User = require('./models/User');
const Cocktail = require('./models/Cocktail');
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@test.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'user',
    avatar: 'no_image_available.jpg'
  }, {
    email: 'admin@test.com',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'admin',
    avatar: 'no_image_available.jpg'
  })

  await Cocktail.create({
    user: user,
    image: 'маргарита.jpg',
    title: 'Margarita',
    recipe: 'Alkogol cocktail',
    ingredients: [{
      name: 'something',
      amount: '0.5'
    }],
    isPublished: false
  }, {
    user: user,
    image: 'лимонад.jpeg',
    title: 'Limonade',
    recipe: 'Cocktail without alkogol',
    ingredients: [{
      name: 'something',
      amount: '0.5'
    }],
    isPublished: false
  }, {
    user: admin,
    image: 'молочный.jpeg',
    title: 'Milk shake',
    recipe: 'Cocktail without alkogol',
    ingredients: [{
      name: 'something',
      amount: '0.5'
    }],
    isPublished: true
  }, {
    user: admin,
    image: 'текила.jpeg',
    title: 'Tekila',
    recipe: 'Cocktail with alkogol',
    ingredients: [{
      name: 'something',
      amount: '0.5'
    }],
    isPublished: true
  });

  await mongoose.connection.close();
}

run().catch(e => console.error(e))