const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MyCocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  cocktail: {
    type: Schema.Types.ObjectId,
    ref: 'Cocktail',
  },
  isPublished: {
    type: Boolean,
    required: true,
    default: false
  },
});

const MyCocktail = mongoose.model('MyCocktail', MyCocktailSchema);
module.exports = MyCocktail;