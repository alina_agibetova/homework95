const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IngredientsSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  amount: String
})

const CocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true,
  },
  image: String,
  recipe: {
    type: String,
    required: true,
  },
  ingredients: {
    type: [IngredientsSchema],
    required: true,
  },
  isPublished: {
    type: Boolean,
    required: true,
    default: false
  },
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;