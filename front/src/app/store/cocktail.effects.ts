import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { CocktailsService } from '../services/cocktails.service';
import { HelpersService } from '../services/helpers.service';
import {
  createCocktailFailure,
  createCocktailPublishFailure,
  createCocktailPublishRequest,
  createCocktailPublishSuccess,
  createCocktailRequest,
  createCocktailSuccess,
  deleteCocktailFailure,
  deleteCocktailRequest,
  deleteCocktailSuccess,
  fetchCocktailFailure,
  fetchCocktailRequest,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchCocktailSuccess
} from './coctail.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CocktailEffects {
  constructor(private actions: Actions,
              private cocktailsService: CocktailsService,
              private helpers: HelpersService) {}

  fetchCocktailById = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailRequest),
    mergeMap(({id}) => this.cocktailsService.getCocktailId(id).pipe(
      map(cocktail => fetchCocktailSuccess({cocktail})),
      catchError(() => of(fetchCocktailFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailsService.getCocktails().pipe(
      map(cocktails => fetchCocktailsSuccess({cocktails})),
      catchError(() => of(fetchCocktailsFailure({error: 'Something went wrong in cocktails'})))
    ))
  ));

  createCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailRequest),
    mergeMap(({cocktailData}) => this.cocktailsService.createCocktail(cocktailData).pipe(
      map(() => createCocktailSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Create cocktail successful')
      }),
      this.helpers.catchServerError(createCocktailFailure)
    ))
  ));

  createPublishCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailPublishRequest),
    mergeMap(({cocktailPublishData}) => this.cocktailsService.cocktailIsPublish(cocktailPublishData).pipe(
      map(() => createCocktailPublishSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Published successful');
      }),
      this.helpers.catchServerError(createCocktailPublishFailure)
    ))
  ));

  deleteCocktail = createEffect(() => this.actions.pipe(
    ofType(deleteCocktailRequest),
    mergeMap(id => this.cocktailsService.deleteCocktail(id.id).pipe(
      map(cocktail => deleteCocktailSuccess({cocktail})),
      tap(() => {
        this.helpers.openSnackbar('Delete cocktail successful')
      }),
      this.helpers.catchServerError(deleteCocktailFailure)
    ))
  ));
}
