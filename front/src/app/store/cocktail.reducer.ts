import { CocktailState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCocktailFailure, createCocktailPublishRequest,
  createCocktailRequest, createCocktailSuccess, deleteCocktailFailure, deleteCocktailRequest, deleteCocktailSuccess,
  fetchCocktailFailure,
  fetchCocktailRequest,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess, fetchCocktailSuccess
} from './coctail.actions';

const initialState: CocktailState = {
  cocktails: [],
  cocktail: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false
}

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailsSuccess, (state, {cocktails }) =>
    ({...state, fetchLoading: false, cocktails})),

  on(fetchCocktailsFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(fetchCocktailRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailSuccess, (state, {cocktail }) =>
    ({...state, fetchLoading: false, cocktail})),

  on(fetchCocktailFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),


  on(createCocktailRequest, state => ({...state, createLoading: true})),

  on(createCocktailSuccess, state => ({...state, createLoading: false})),

  on(createCocktailFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
  on(createCocktailPublishRequest, state => ({...state, publishLoading: false})),

  on(deleteCocktailRequest, state => ({...state, fetchLoading: true})),
  on(deleteCocktailSuccess, (state, {cocktail}) =>
    ({...state, fetchLoading: false, cocktail})),

  on(deleteCocktailFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),
);
