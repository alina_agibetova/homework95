import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { HelpersService } from '../services/helpers.service';
import { CocktailsService } from '../services/cocktails.service';
import {
  createMyCocktailFailure,
  createMyCocktailRequest,
  createMyCocktailSuccess,
  fetchMyCocktailsFailure,
  fetchMyCocktailsRequest,
  fetchMyCocktailsSuccess
} from './myCocktail.actions';

@Injectable()
export class MyCocktailEffects{
  constructor(private actions: Actions,
              private cocktailsService: CocktailsService,
              private helpers: HelpersService) {
  }

  createMyCocktail = createEffect(() => this.actions.pipe(
    ofType(createMyCocktailRequest),
    mergeMap(({MyCocktailData}) => this.cocktailsService.createMyCocktail(MyCocktailData).pipe(
      map(()=> createMyCocktailSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Successful')
      }),
      this.helpers.catchServerError(createMyCocktailFailure)
    ))
  ));

  fetchMyCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchMyCocktailsRequest),
    mergeMap(() => this.cocktailsService.getMyCocktails().pipe(
      map(myCocktails => fetchMyCocktailsSuccess({myCocktails})),

      catchError(() => of(fetchMyCocktailsFailure({error: 'Something went wrong'})))
    ))
  ));

}
