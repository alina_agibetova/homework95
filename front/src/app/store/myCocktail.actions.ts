import { createAction, props } from '@ngrx/store';
import { MyCocktail } from '../models/myCocktail.model';

export const createMyCocktailRequest = createAction(
  '[MyCocktail] Create Request',
  props<{MyCocktailData: string}>());
export const createMyCocktailSuccess = createAction(
  '[MyCocktail] Create Success'
);
export const createMyCocktailFailure = createAction(
  '[MyCocktail] Create Failure', props<{error: string}>());


export const fetchMyCocktailsRequest = createAction(
  '[MyCocktail] Fetch Request',
);

export const fetchMyCocktailsSuccess = createAction(
  '[MyCocktail] Fetch Success',
  props<{myCocktails: MyCocktail[]}>()
);

export const fetchMyCocktailsFailure = createAction(
  '[MyCocktail] Fetch Failure',
  props<{error: string}>()
);
