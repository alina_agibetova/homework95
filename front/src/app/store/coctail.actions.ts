import { createAction, props } from '@ngrx/store';
import { Cocktail, CocktailData, CocktailPublishData } from '../models/cocktail.model';

export const fetchCocktailRequest = createAction(
  '[Cocktail] Fetch Request',
  props<{id: string}>()
);
export const fetchCocktailSuccess= createAction(
  '[Cocktail] Fetch Success',
  props<{cocktail: Cocktail}>()
);
export const fetchCocktailFailure = createAction(
  '[Cocktail] Fetch Failure',
  props<{error: string}>()
);


export const fetchCocktailsRequest = createAction(
  '[Cocktails] Fetch Request'
);
export const fetchCocktailsSuccess= createAction(
  '[Cocktails] Fetch Success',
  props<{cocktails: Cocktail[]}>()
);
export const fetchCocktailsFailure = createAction(
  '[Cocktails] Fetch Failure',
  props<{error: string}>()
);



export const createCocktailRequest = createAction(
  '[Cocktail] Create Request',
  props<{cocktailData: CocktailData}>()
);
export const createCocktailSuccess = createAction(
  '[Cocktail] Create Success'
);
export const createCocktailFailure = createAction(
  '[Cocktail] Create Failure',
  props<{error: string}>()
);


export const createCocktailPublishRequest = createAction(
  '[Cocktail] Create Request',
  props<{cocktailPublishData: CocktailPublishData}>()
);
export const createCocktailPublishSuccess = createAction(
  '[Cocktail] Create Success'
);
export const createCocktailPublishFailure = createAction(
  '[Cocktail] Create Failure',
  props<{error: string}>()
);

export const deleteCocktailRequest = createAction(
  '[Cocktail] Delete Request',
  props<{id: string}>()
);

export const deleteCocktailSuccess = createAction(
  '[Cocktail] Delete Success',
  props<{cocktail: Cocktail}>()
);

export const deleteCocktailFailure = createAction(
  '[Cocktail] Delete Failure',
  props<{error: string}>()
);
