import { LoginError, RegisterError, User } from '../models/user.model';
import { Cocktail } from '../models/cocktail.model';
import { MyCocktail } from '../models/myCocktail.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}

export type CocktailState = {
  cocktails: Cocktail[];
  cocktail: Cocktail | null,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean,
}

export type MyCocktailState = {
  myCocktails: MyCocktail[],
  myCocktail: MyCocktail | null,
  createLoading: boolean,
  createError: null | string,
  fetchLoading: boolean,
  fetchError: null | string,
}


export type AppState = {
  users: UsersState,
  cocktails: CocktailState,
  myCocktails: MyCocktailState,
}
