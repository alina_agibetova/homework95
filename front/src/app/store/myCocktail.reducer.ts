import { MyCocktailState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createMyCocktailFailure,
  createMyCocktailRequest,
  createMyCocktailSuccess,
  fetchMyCocktailsFailure,
  fetchMyCocktailsRequest,
  fetchMyCocktailsSuccess
} from './myCocktail.actions';

const initialState: MyCocktailState = {
  myCocktails: [],
  myCocktail: null,
  createLoading: false,
  createError: null,
  fetchLoading: false,
  fetchError: null,
}

export const myCocktailsReducer = createReducer(
  initialState,
  on(createMyCocktailRequest, state => ({...state, createLoading: true})),

  on(createMyCocktailSuccess, state => ({...state, createLoading: false})),

  on(createMyCocktailFailure, (state, {error}) =>
    ({...state, createLoading: false, createError: error})),

  on(fetchMyCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchMyCocktailsSuccess, (state, {myCocktails}) =>
    ({...state, fetchLoading: false, myCocktails})),

  on(fetchMyCocktailsFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),

);
