import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Cocktail, CocktailData } from '../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { createCocktailRequest, fetchCocktailsRequest } from '../store/coctail.actions';
import { User } from '../models/user.model';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.sass']
})
export class NewCocktailComponent implements OnInit {
  form!: FormGroup;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  cocktails: Observable<Cocktail[]>;
  user!: Observable<User | null>;
  id!: string;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.cocktails.createLoading);
    this.error = store.select(state => state.cocktails.createError);
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());
    this.user.subscribe(user => {
      this.id = <string>user?._id;
    })

    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
      recipe: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
    })
  }



  onSubmit() {
    const cocktailData: CocktailData = {
      user: this.id,
      title: this.form.value.title,
      recipe: this.form.value.recipe,
      image: this.form.value.image,
      ingredients: this.form.value.ingredients,
      isPublished: false,
    }
    this.store.dispatch(createCocktailRequest({cocktailData}))
  }


  addStep() {
    const nameIng = <FormArray>this.form.get('ingredients');
    const group = new FormGroup({
      name: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
    });
    nameIng.push(group);
  }

  getIngredients() {
    const nameIng = <FormArray>(this.form.get('ingredients'));
    return nameIng.controls;
  }
}
