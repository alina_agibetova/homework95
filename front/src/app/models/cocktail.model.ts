export class Cocktail {
  constructor(
    public _id: string,
    public title: string,
    public user: string,
    public image: string,
    public ingredients: [{
      name: string,
      amount: string
    }],
    public recipe: string,
    public isPublished: boolean
  ) {}
}

export class Ingredients {
  constructor(
    public name: string,
    public amount: string
  ) {
  }
}

export interface CocktailData {
  [key: string]: any;
  user: string;
  title: string;
  image: File | null;
  ingredients: [{
    name: string,
    amount: string
  }];
  recipe: string,
  isPublished: boolean;
}

export interface ApiCocktailData {
  _id: string,
  title: string,
  user: string,
  image: string,
  ingredients: [{
    name: string,
    amount: string
  }],
  recipe: string,
  isPublished: boolean
}

export interface CocktailPublishData {
  id: string,
  isPublished: boolean,
}

