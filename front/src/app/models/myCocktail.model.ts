export class MyCocktail{
  constructor(
    public _id: string,
    public user: string,
    public cocktail: {
      _id: string,
      title: string,
      image: string
    },
  ) {}
}

export interface MyCocktailData {
  user: string,
  cocktail: string,
  token: string,
}

export interface ApiMyCocktailData {
  _id: string,
  user: string,
  cocktail: {
    _id: string,
    title: string,
    image: string
  },
  token: string,
}
