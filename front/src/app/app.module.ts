import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { CocktailComponent } from './cocktails/cocktail/cocktail.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { CenterCardComponent } from './ui/center-card/center-card.component';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ImagePipe } from './pipes/image.pipe';
import { HasRolesDirective } from './directives/has-roles.directive';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { AppStoreModule } from './app-store.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { AuthInterceptor } from './auth.interceptor';
import { environment } from '../environments/environment';
import { MycocktailsComponent } from './mycocktails/mycocktails.component';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [{
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.fbAppId, {
      scope: 'email, public_profile'
    })
  }
  ]
}

@NgModule({
  declarations: [
    AppComponent,
    CocktailsComponent,
    CocktailComponent,
    LoginComponent,
    RegisterComponent,
    NewCocktailComponent,
    CenterCardComponent,
    LayoutComponent,
    FileInputComponent,
    ImagePipe,
    HasRolesDirective,
    MycocktailsComponent,
  ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      LayoutModule,
      MatToolbarModule,
      MatButtonModule,
      MatSidenavModule,
      MatIconModule,
      MatListModule,
      MatMenuModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MatFormFieldModule,
      MatInputModule,
      FlexLayoutModule,
      MatCardModule,
      AppStoreModule,
      MatSnackBarModule,
      MatProgressSpinnerModule,
      SocialLoginModule,

    ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
