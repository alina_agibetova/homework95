import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail } from '../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { ActivatedRoute } from '@angular/router';
import { CocktailsService } from '../services/cocktails.service';
import { fetchCocktailRequest, fetchCocktailsRequest } from '../store/coctail.actions';
import { MyCocktail } from '../models/myCocktail.model';
import { fetchMyCocktailsRequest } from '../store/myCocktail.actions';

@Component({
  selector: 'app-mycocktails',
  templateUrl: './mycocktails.component.html',
  styleUrls: ['./mycocktails.component.sass']
})
export class MycocktailsComponent implements OnInit {
  myCocktails: Observable<MyCocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private cocktailsService: CocktailsService) {
    this.myCocktails = store.select( state => state.myCocktails.myCocktails);
    this.loading = store.select(state => state.myCocktails.fetchLoading);
    this.error = store.select(state => state.myCocktails.fetchError)
  }

  ngOnInit(): void {
    this.store.dispatch(fetchMyCocktailsRequest());

  }
}
