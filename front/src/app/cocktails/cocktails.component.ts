import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail } from '../models/cocktail.model';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '../store/types';
import { Store } from '@ngrx/store';
import { CocktailsService } from '../services/cocktails.service';
import { fetchCocktailRequest, fetchCocktailsRequest } from '../store/coctail.actions';
import { MyCocktail } from '../models/myCocktail.model';
import { createMyCocktailRequest } from '../store/myCocktail.actions';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.sass']
})
export class CocktailsComponent implements OnInit {
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  MyCocktails: Observable<MyCocktail[]>

  constructor(private store: Store<AppState>, private route: ActivatedRoute, private cocktailsService: CocktailsService) {
    this.MyCocktails = store.select(state => state.myCocktails.myCocktails)
    this.cocktails = store.select( state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError)
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());

  }

  onClick(_id: string) {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchCocktailRequest({id: params['id']}));
    });
  }

  addTrack(id: string) {
    this.store.dispatch(createMyCocktailRequest({MyCocktailData: id}))
  }
}
