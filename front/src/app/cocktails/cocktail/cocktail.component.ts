import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/user.model';
import { ApiCocktailData, Cocktail, CocktailPublishData } from '../../models/cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute } from '@angular/router';
import { createCocktailPublishRequest, deleteCocktailRequest, fetchCocktailRequest } from '../../store/coctail.actions';

@Component({
  selector: 'app-cocktail',
  templateUrl: './cocktail.component.html',
  styleUrls: ['./cocktail.component.sass']
})
export class CocktailComponent implements OnInit {
  user: Observable<User | null>;
  cocktail: Observable<Cocktail | null>;
  deleteCocktail: Observable<Cocktail | null>
  loading: Observable<boolean>;
  error: Observable<null | string>;
  userSub!: Subscription;
  publishCocktail: Observable<boolean>;
  thisCocktail!: ApiCocktailData;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.cocktail = store.select( state => state.cocktails.cocktail);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
    this.user = store.select(state => state.users.user);
    this.deleteCocktail = store.select(state => state.cocktails.cocktail);
    this.publishCocktail = store.select(state => state.cocktails.publishLoading)

  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchCocktailRequest({id: params['id']}));
    });
    this.cocktail.subscribe(cocktail => {
      this.thisCocktail = <ApiCocktailData>cocktail;
    });
  }

  onPublish(id: string){
    const publish: CocktailPublishData = {
      id: id,
      isPublished: true
    }
    this.store.dispatch(createCocktailPublishRequest({cocktailPublishData: publish}))
  }

  onDelete(_id: string) {
    this.store.dispatch(deleteCocktailRequest({id: _id}))
  }

  // onClick(_id: string) {
  //   this.route.params.subscribe(params => {
  //     this.store.dispatch(fetchCocktailRequest({user: params['id']}));
  //   });
  // }
}
