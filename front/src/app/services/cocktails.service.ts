import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';
import { ApiCocktailData, CocktailData, CocktailPublishData } from '../models/cocktail.model';
import { ApiMyCocktailData } from '../models/myCocktail.model';

@Injectable({
  providedIn: 'root'
})
export class CocktailsService {

  constructor(private http: HttpClient) { }

  getCocktails() {
    return this.http.get<ApiCocktailData[]>('http://localhost:8000/cocktails').pipe(
      map( response => {
        return response
      })
    )
  }

  getCocktailId(id: string){
    return this.http.get<ApiCocktailData>(`http://localhost:8000/cocktails/${id}`).pipe(
      map(result => {
        console.log(result)
        return result
      })
    );
  }

  createCocktail(cocktailData: CocktailData){
    const formData = new FormData();

    Object.keys(cocktailData).forEach(key => {
      if (cocktailData[key] !== null){
        if (key !== 'ingredients') {
          formData.append(key, cocktailData[key]);
        } else {
          formData.append(key, JSON.stringify(cocktailData[key]));
        }
      }
    });

    return this.http.post('http://localhost:8000/cocktails', formData);
  }

  cocktailIsPublish(cocktailPublishData: CocktailPublishData){
    const publish = {
      isPublished: true
    }
    return this.http.post<CocktailPublishData>(`http://localhost:8000/cocktails/${cocktailPublishData.id}/publish`, publish)
  }

  deleteCocktail(id: string){
    return this.http.delete<ApiCocktailData>(`http://localhost:8000/cocktails/${id}`);
  }


  createMyCocktail(cocktail: string){
    return this.http.post<ApiMyCocktailData>('http://localhost:8000/my_cocktails', {cocktail})

  }

  getMyCocktails(){
    return this.http.get<ApiMyCocktailData[]>('http://localhost:8000/my_cocktails', ).pipe(
      map(result => {
        console.log(result)
        return result
      })
    );
  }

}


