import { NgModule } from '@angular/core';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { usersReducer } from './store/users.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/users.effects';
import { localStorageSync } from 'ngrx-store-localstorage';
import { cocktailsReducer } from './store/cocktail.reducer';
import { CocktailEffects } from './store/cocktail.effects';
import { myCocktailsReducer } from './store/myCocktail.reducer';
import { MyCocktailEffects } from './store/myCocktail.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];
const reducers = {
  users: usersReducer,
  cocktails: cocktailsReducer,
  myCocktails: myCocktailsReducer
}

const effects = [UsersEffects, CocktailEffects, MyCocktailEffects];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule]
})
export class AppStoreModule{}
