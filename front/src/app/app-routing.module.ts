import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { CocktailComponent } from './cocktails/cocktail/cocktail.component';
import { RoleGuardService } from './services/role-guard.service';
import { MycocktailsComponent } from './mycocktails/mycocktails.component';

const routes: Routes = [
  {
    path: '',
    component: CocktailsComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['user', 'admin']}
  },
  {
    path: 'cocktail/:id',
    component: CocktailComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['user', 'admin']}
  },
  {path: 'myCocktail', component: MycocktailsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'new-cocktail', component: NewCocktailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
